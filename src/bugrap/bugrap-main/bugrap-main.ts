/// <reference path="../lib/polymer.d.ts" />

(function() {

    class BugRapMain {

        private _component: any;
        private _distribution;
        
        public constructor(component:any) {
            this._component = component;
            
            var stats = component.$.tasks.stats;
            component.$.distribution.data = [
                stats.new, stats.open, stats.assigned, stats.closed
            ];
        }

    }

    Polymer({
        is: 'bugrap-main',

        properties: {

        },

        ready: function() {
            this._instance = new BugRapMain(this);
        },

        _instance: null
    });

})();