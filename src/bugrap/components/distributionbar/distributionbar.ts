/// <reference path="../../lib/polymer.d.ts" />

(function() {

    /**
     * Simple CSS rule generation helper
     */
    class CSSRule {
        
        private name: string;
        private attrs: any[];

        constructor(name: string) {
            this.name = name;
            this.attrs = [];
        }

        public setAttr(name: string, value: string) {
            this.attrs.push({ name: name, value: value });
        }

        public toString(): string {
            if(this.name == '') {
                return this.innerToString();
            }
            var s = this.name + ' { ';
            for (var i = 0; i < this.attrs.length; ++i) {
                var a = this.attrs[i];
                s += a.name + ": " + a.value + "; ";
            }
            return s + "} ";
        } 
        
        private innerToString(): string {
            var s = "";
            for (var i = 0; i < this.attrs.length; ++i) {
                var a = this.attrs[i];
                s += a.name + ": " + a.value + "; ";
            }
            return s;
        }
    }
    
    /**
     * Distribution bar logic
     */
    class DistributionBar {

        private container: HTMLElement;
        private data: number[];
        private distribution: number[];
        private elements: HTMLDivElement[];

        constructor(e: any = null) {
            this.container = e.$.content;
            this.data = [100];
            this.distribution = [1];
            this.elements = [];
            this.redraw();
        }

        public setData(data: number[]): void {
            if (data != null) {
                this.data = data;
            } else this.data = [0];
            this.redraw();
        }

        private recalculateDistribution(): void {
            var total = 0;
            for (var i = 0; i < this.data.length; ++i) {
                total += this.data[i];
            }

            this.distribution = [];
            for (var i = 0; i < this.data.length; ++i) {
                this.distribution[i] = this.data[i] / total;
            }
        }

        private generateElements(): void {
            this.elements = [];
            for (var i = 0; i < this.data.length; ++i) {
                var e = document.createElement('div');
                e.className = 'dist-element dist-' + i;
                e.innerText = '' + this.data[i];
                this.elements.push(e);
            }
        }

        private generateStyle(): void {
            
            // Common formatting for all divs inside..
            var divRule = new CSSRule('');
            divRule.setAttr('margin', '5px');
            divRule.setAttr('border', '0px');
            divRule.setAttr('padding', '0px');

            var cellRules: CSSRule[] = [];
            for (var i = 0; i < this.data.length; ++i) {
                var rule = new CSSRule('');
                var size = (this.distribution[i] * 100.0) | 0;
                rule.setAttr('width',size + "%");
                rule.setAttr('text-overflow', 'ellipsis');
                rule.setAttr('overflow', 'hidden');
                rule.setAttr('height','100%');
                rule.setAttr('min-width','32px');
                rule.setAttr('text-align','center');
                
                var bg = 9 - i;
                var bgs = '' + bg + 'f';
                
                rule.setAttr('background-color', '#' + bgs + bgs + bgs); 
                cellRules.push(rule);
            }
            
            if(cellRules.length != 1) {
                cellRules[0].setAttr('border-radius', '5px 0px 0px 5px');
                cellRules[cellRules.length - 1].setAttr('border-radius', '0px 5px 5px 0px');
            } else {
                cellRules[0].setAttr('border-radius', '5px 5px 5px 5px');
            }

            var s = divRule.toString();
            for (var i = 0; i < cellRules.length; ++i) {
                s += cellRules[i].toString();
                this.elements[i].setAttribute('style',cellRules[i].toString());
            }

            this.container.setAttribute('style', divRule.toString());
        }

        private redraw(): void {
            this.container.innerHTML = '';

            this.recalculateDistribution();
            this.generateElements();
            this.generateStyle();

            for (var i = 0; i < this.elements.length; ++i) {
                this.container.appendChild(this.elements[i]);
            }
        }
    }
  
    //========================================================================
    // Register Polymer element
    //========================================================================
    
    // TS doesn't seem to like having undeclared props
    // in the object passed to Polymer, so we cast to 'any' to
    // disable object verification by the compiler
    
    Polymer(<any>{
        is: 'distribution-bar',

        properties: {
            data: {
                type: Array,
                value: [100],
                readOnly: false,
                observer: '_dataChanged'
            }
        },

        _dataChanged: function(newData, oldData) {
            if (this._instance == null) return;
            this._instance.setData(newData);
        },

        ready: function() {
            this._instance = new DistributionBar(this);
        },

        _instance: null
    });
})();