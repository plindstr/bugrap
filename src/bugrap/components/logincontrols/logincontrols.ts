/// <reference path="../../lib/polymer.d.ts" />

(function() {
    
    class LoginControls {
     
        private _component: any;
        
        public constructor(component:any) {
            this._component = component;
        }
        
    }
    
    Polymer(<any>{
        is: 'login-controls',

        properties: {
        },

        ready: function() {
            this._instance = new LoginControls(this);
        },

        _instance: null
    });
})();