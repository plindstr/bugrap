/// <reference path="../../lib/polymer.d.ts" />

(function() {

    class TaskList {

        private _component: any;
        private _grid: any;

        private _columns: Object;
        private _data: Object[];
        
        public constructor(e: any) {
            this._component = e;
            this._grid = e.$.content;
            this.generateData();
            
            this._grid.columns = this._columns;
            this._grid.items = this._data;
        }

        private generateData():void {

            var pickRandom = (data:any[]):any => {
                return data[(Math.random() * data.length) | 0];
            };
            
            function getPriority() {
                return pickRandom([
                    "Low",
                    "Medium",
                    "High",
                    "Severe",
                    "Critical"
                ]);
            }
            
            function getType() {
                return pickRandom([
                    "Bug",
                    "Feature",
                    "Enhancement"
                ]);
            }
            
            function getSummary() {
                if(Math.random() > 0.8) {
                    return pickRandom([
                        "ENLARGE YOUR P3N15 NOW!!!1",
                        "GET ANY PASSPORT TODAY!!",
                        "FREE CERTIFICATIONS AND DIPLOMAS HERE",
                        "HI GORGEOUS I JUST SAW YOUR PROFILE",
                        "IF THIS POST GETS 10000 LIKES BILL GATES WILL DONATE HIS KIDNEY TO AIDS RESEARCH",
                        "*** SPAM DELETED ***"
                    ]);
                }
                
                var p1 = pickRandom([
                    "Grid",
                    "Table",
                    "Button",
                    "ComboBox",
                    "CheckBox",
                    "SQLContainer",
                    "Window",
                    "HorizontalLayout",
                    "VerticalLayout",
                    "FormLayout",
                    "CSSLayout",
                    "AbsoluteLayout" 
                ]);
                
                var p2 = pickRandom([
                    "does not work",
                    "is missing documentation",
                    "freezes unexpectedly",
                    "renders wrong",
                    "is not compatible with LifeRay"
                ]);
                
                return "The Vaadin " + p1 + " component " + p2; 
            }
            
            function getAssigned() {
                return pickRandom([
                    "",
                    "",
                    "",
                    "",
                    "",
                    "",
                    "",
                    "",
                    "",
                    "",
                    "",
                    "Del Dileo",
                    "Jamaal Johansen",
                    "Zackary Zhou",
                    "Ernest Edison",
                    "Juan Jarrett",
                    "Marcelo Mccarty",
                    "Cedric Collin",
                    "Hugo Hui",
                    "Gavin Grice",
                    "Luciano Lake",
                    "Virgil Vencill",
                    "Burl Bisceglia",
                    "Bobbie Baldree",
                    "Douglass Dupuy",
                    "Arlie Anguiano",
                    "Keenan Kinnaird",
                    "Rosario Reneau",
                    "Efren Elgin",
                    "Jude Jeansonne",
                    "Branden Binette"
                ]);
            }
            
            function getLastModified() {
                return pickRandom([
                    "Today",
                    "Today",
                    "Today",
                    "Yesterday",
                    "Yesterday",
                    "Yesterday",
                    "Yesterday",
                    "Last week",
                    "Last week",
                    "Last week",
                    "Last week",
                    "Last week",
                    "Last month",
                    "Last month",
                    "Last month",
                    "Last month",
                    "Last month",
                    "Last month",
                    "Last month",
                    "Last year",
                    "Last year",
                    "Last year",
                    "Last year",
                    "Last year",
                    "Last year",
                    "Last year",
                    "Monday",
                    "Tuesday",
                    "Wednesday",
                    "Thursday",
                    "Friday",
                    "Saturday",
                    "Sunday"
                ]);
            }
            
            function getReported() {
                return new Date((Math.random() * 0xffffffff) | 0);
            }
            
            function getStatus(assigned:string) {
                if(assigned == '') {
                    return pickRandom([
                        "New",
                        "Open"
                    ]);
                }
                return pickRandom([
                    "Assigned",
                    "Closed"
                ]);
            }
            
            this._columns = [
                { headerContent: "PRIORITY" },
                { headerContent: "TYPE" },
                { headerContent: "SUMMARY" },
                { headerContent: "ASSIGNED TO" },
                { headerContent: "STATUS" },
                { headerContent: "LAST MODIFIED" },
                { headerContent: "REPORTED" }
            ];
            
            this._data = [];
            for(var i = 0; i < 256; ++i) {
                var assigned = getAssigned();
                this._data.push([
                    getPriority(),
                    getType(),
                    getSummary(),
                    assigned,
                    getStatus(assigned),
                    getLastModified(),
                    getReported()
                ]);
            }
            
            this.updateStats();
            
        }
        
        private updateStats(): void  {

            var newcount = 0;
            var opencount = 0;
            var assignedcount = 0;
            var closedcount = 0;
            
            for(var i = 0; i < this._data.length; ++i) {
                var status = this._data[i][4];
                switch(status) {
                    case 'New':
                        newcount++;
                    break;
                    case 'Open':
                        opencount++;
                    break;
                    case 'Assigned':
                        assignedcount++;
                    break;
                    case 'Closed':
                        closedcount++;
                    break;
                }
            }
            
            this._component._setStats({
                new: newcount,
                open: opencount,
                assigned: assignedcount,
                closed: closedcount
            });
            
        }
        
    }

    Polymer(<any>{
        is: 'task-list',

        properties: {
            stats: {
                type: Object,
                readOnly: true,
                notify: true
            }
        },

        ready: function() {
            this._instance = new TaskList(this);
        },

        _instance: null
    });
})();