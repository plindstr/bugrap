/// <reference path="../../lib/polymer.d.ts" />

(function() {

    class ReportList {

        private _component: any;

        public constructor(component: any) {
            this._component = component;
        }

    }

    Polymer(<any>{
        is: 'report-list',

        properties: {
        },

        ready: function() {
            this._instance = new ReportList(this);
        },

        _instance: null
    });
})();