/// <reference path="../../lib/polymer.d.ts" />

(function() {

    class ProjectName {

        private _component: any;

        public constructor(component: any) {
            this._component = component;
        }

    }

    Polymer(<any>{
        is: 'project-name',

        properties: {
        },

        ready: function() {
            this._instance = new ProjectName(this);
        },

        _instance: null
    });
})();