#
# BugRap project main Makefile
#

BUILDDIR=build
DISTDIR=dist

all: src build
	cd src && make all
	mkdir -p ${DISTDIR}
	cp -r ${BUILDDIR}/favicon ${DISTDIR}/
	cp ${BUILDDIR}/index.html ${DISTDIR}/

bower:
	cd src && make bower
	cd ${BUILDDIR} && bower update

clean:
	rm -rf build
	rm -rf dist
